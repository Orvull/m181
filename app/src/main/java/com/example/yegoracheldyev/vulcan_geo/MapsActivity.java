package com.example.yegoracheldyev.vulcan_geo;
import android.location.Location;
import android.os.Bundle;
import android.app.FragmentManager;
import android.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.app.ActionBar;
import android.view.Window;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;


public class MapsActivity extends AppCompatActivity {


    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_button_back);
        getSupportActionBar().setTitle(null);
       /* getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/

       // getSupportActionBar().setLogo(R.mipmap.ic_launcher);
        setUpMapIfNeeded();
        setUpMap();
    }
   @Override

    public boolean onCreateOptionsMenu(Menu menu) {

     getMenuInflater().inflate(R.menu.second_bar, menu);
       return true;


   }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.type:
                return true;
            case R.id.normal:
                item.setChecked(true);
                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                return true;
            case R.id.hybrid:
                item.setChecked(true);
                mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                return true;
            case R.id.terrain:
                item.setChecked (true);
                mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                return true;
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


   /* public void sendMessage(View view) {

        Intent intent = new Intent(this, MainActivity.class);

        startActivity(intent);
    }*/



   @Override
    protected void onResume(){
        super.onResume();
        setUpMapIfNeeded();
        }

private void setUpMapIfNeeded(){

        if(mMap==null){
        mMap=((SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.map))
        .getMap();

        if(mMap!=null){
        setUpMap();
        // MAP_TYPE_TERRAIN, MAP_TYPE_HYBRID and MAP_TYPE_NONE
            //  mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        }
        }

        }

public void setUpMap(){
        mMap.setMyLocationEnabled(true);
        mMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {


            public void onMyLocationChange(Location location) {
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 17));


                mMap.addMarker(new MarkerOptions()
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_play_light))
                        .position(new LatLng(location.getLatitude(), location.getLongitude()))
                        .flat(true)
                        .rotation(200));

                CameraPosition cameraPosition;
                cameraPosition = CameraPosition.builder()
                        .target(new LatLng(location.getLatitude(), location.getLongitude()))
                        .zoom(17)
                        .bearing(90)
                        .build();

                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition),
                        3000, null);


            }
        });


    }



}

